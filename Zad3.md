---
author: Szewczyk Bartłomiej
title: Barcelona miasto słońca
date: 03.11.2023
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{listings}

---

## Slajd 1: Witamy w Barcelonie!

Barcelona to piękne miasto położone na wschodnim wybrzeżu Hiszpanii. Znane ze swojej architektury, plaż i niezapomnianej atmosfery.

---

## Slajd 2: Sagrada Familia

![Sagrada Familia](sagrada_familia.jpg)
Jedna z najbardziej ikonicznych budowli Barcelony. Sagrada Familia to niezwykła świątynia zaprojektowana przez Antonio Gaudíego.

---

## Slajd 3: Park Güell

![Park Güell](park_guell.jpg)
Park Güell to miejsce pełne kolorów, fantastycznej architektury i zapierających dech widoków na miasto.

---

## Slajd 4: La Rambla

La Rambla to słynna aleja w samym sercu Barcelony, pełna życia, sklepów, restauracji i artystów ulicznych.

---

## Slajd 5: Plaże Barcelony

Barcelona oferuje piękne plaże, idealne do relaksu i kąpieli słonecznych. Woda Morza Śródziemnego zapewnia orzeźwienie.

---

## Slajd 6: Kulinaria Barcelony

Barcelona to raj dla smakoszy. Próbuj tradycyjnych tapas, paelli i pysznych owoców morza.

---

## Slajd 7: Camp Nou

![Camp Nou](camp_nou.jpg)
Dla miłośników piłki nożnej nie może zabraknąć wizyty na Camp Nou, stadionie drużyny FC Barcelona.

---

## Slajd 8: Barri Gòtic - Dzielnica Gotycka

Barri Gòtic to najstarsza dzielnica Barcelony, pełna wąskich uliczek, zabytków i tajemniczych zakątków.

---

# Dziękuję za uwagę!

Sprawdź więcej informacji o Barcelonie na [*Wikipedii*](https://pl.wikipedia.org/wiki/Barcelona).

Żródła:

1. *[Sagrada Familia](https://turystyka.wp.pl/sagrada-familia-6047130369548929c)*
2. *[Park Guell](https://www.getyourguide.pl/guell-park-l3032/)*
3. *[Camp Nou](https://pl.wikipedia.org/wiki/Camp_Nou)*
